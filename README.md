
# Overview
Dimensions (L x W, millimeters): 43 x 57.

![3d view top](bom/zx-mini-pcb-debug-module-cpu-v1_3dT.png)
![3d view bottom](bom/zx-mini-pcb-debug-module-cpu-v1_3dB.png)
