
|       | References	  | Value	        | Footprint	                  | Quantity |
|-------|---------------|---------------|-----------------------------|----------|
| 1		| C4	          | 10uF	        | CP_EIA-3216-18_Kemet-A	    | 1|
| 2		| C5, C6, C7, C9|	0.1uF|	C_0805_2012Metric	|4|
| 3		| C1, C2, C3|	0.01uF|	C_0805_2012Metric	|3|
| 4		| C10, C11|	22p|	C_0805_2012Metric	|2|
| 5		| C8|	1uF|	C_0805_2012Metric	|1|
| 6		| R4, R5, R6, R7, R10|	4,7k|	R_0805_2012Metric	|5|
| 7		| R1, R2, R3|	220|	R_0805_2012Metric	|3|
| 8		| R8, R9|	470|	R_0805_2012Metric	|2|
| 9		| R11|	10k|	R_0805_2012Metric	|1|
| 10		|	L1|	LQM21DN470N|	L_0805_2012Metric	|1|
| 11		|	D3, D4, D5|	BZX384-B3V6|	D_SOD-323	|3|
| 12		|	D1|	TO-1608BC-PG|	LED_0603_1608Metric	|1|
| 13		|	D2|	TO-1608BC-MRE|	LED_0603_1608Metric	|1|
| 14		|	D6|	STM32L010K8|	LQFP-32_7x7mm_P0.8mm	|1|
| 15  	|	Y1|	32.768kHz|	Crystal_AT310_D3.0mm_L10.0mm_Horizontal	|1|
| 16  	|	SW4, SW6|	DS1040-01RN|	SW_DIP_SPSTx01_Slide_9.78x4.72mm_W7.62mm_P2.54mm	|2|
| 17  	|	SW1, SW2, SW3|	DS1040-02RN|	SW_DIP_SPSTx02_Slide_9.78x7.26mm_W7.62mm_P2.54mm	|3|
| 18  	|	SW5|	1437566-3 (FSMSM)|	SW_SPST_FSMSM	|1|
| 19		|	J3|	IDC-10|	IDC-Header_2x05_P2.54mm_Vertical	|1|
| 20		|	J7, J8|	PLS-3	|PinHeader_1x03_P2.54mm_Vertical	|2|
| 21		|	J6|	PLS-5|	PinHeader_1x05_P2.54mm_Vertical	|1|
| 22		|	J4, J5|	PLS-16|	PinHeader_1x16_P2.54mm_Vertical	|2|
| 23		|	J1, J2|	PBS-16|	PinSocket_1x16_P2.54mm_Vertical	|2|
